package com.boonanan.lab11;

public class Crocodie extends Animal implements Swimable, Crawlable{

    public Crocodie(String name) {
        super(name, 4);
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + " crawl.");
    }
    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
    }
    @Override
    public void waterbreatheable() {
        System.out.println(this.toString() + " waterbreathe.");
    }
    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }
    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }
    @Override
    public String toString() {
        return "Crocodie(" + this.getName() + ")";
    }
    
}
