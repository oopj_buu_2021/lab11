package com.boonanan.lab11;

public abstract class Animal {
    private String name;
    private int numOfLegs;

    public Animal(String name, int numOfLegs) {
        this.name = name;
        this.numOfLegs = numOfLegs;
    }

    public String getName() {
        return name;
    }
    public int getNumOfLegs() {
        return numOfLegs;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLegs(int numOfLegs) {
        this.numOfLegs = numOfLegs;
    }

    public abstract void eat();
    public abstract void sleep();

    @Override
    public String toString() {
        return "Animal(" + name + ") has " + numOfLegs + "Legs";
    }

}
