package com.boonanan.lab11;

public class App {
    public static void main( String[] args ){
        Human humam1 = new Human("Adam");
        humam1.eat();
        humam1.sleep();
        humam1.walk();
        humam1.run();

        Bird bird1 = new Bird("Parrot");
        bird1.fly();
        bird1.takeoff();
        bird1.landing();

        Snake snake1 = new Snake("Naga");
        snake1.crawl();

        Plane plane1 = new Plane("Boeing", "ROLLS ROYCE");
        plane1.fly();
        plane1.takeoff();
        plane1.landing();

        Submarine submarine1 = new Submarine("Prayut", "Nuclear Reactor");
        submarine1.swim();
        submarine1.waterbreatheable();

        Flyable[] flyableObject = {bird1,plane1};
        for (int i = 0; i < flyableObject.length; i++) {
            flyableObject[i].fly();
            flyableObject[i].takeoff();
            flyableObject[i].landing();
        }

        Walkable[] walkableObject = {humam1};
        for (int i = 0; i < flyableObject.length-1; i++) {
            walkableObject[i].walk();
            walkableObject[i].run();
        }

        Swimable[] swimablesObject = {submarine1};
        for (int i = 0; i < swimablesObject.length; i++) {
            swimablesObject[i].swim();
            swimablesObject[i].waterbreatheable();
        }

        Crawlable[] crawlablesObject = {snake1};
        for (int i = 0; i < crawlablesObject.length; i++) {
            crawlablesObject[i].crawl();
        }
    }
}
