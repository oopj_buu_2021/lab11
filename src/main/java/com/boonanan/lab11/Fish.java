package com.boonanan.lab11;

public class Fish extends Animal implements Swimable {

    public Fish(String name) {
        super(name, 0);
    }
    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
    }
    @Override
    public void waterbreatheable() {
        System.out.println(this.toString() + " waterbreathe.");
    }
    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }
    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }
    @Override
    public String toString() {
        return "Fish(" + this.getName() + ")";
    }
}
