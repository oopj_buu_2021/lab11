package com.boonanan.lab11;

public interface Walkable {
    public void walk();
    public void run();
}
